My "hello world" style application looks as follows:

![Image1](raw/master/HelloWorld_LauraRamstad.png)

After playing around with the APIs a bit, I created this simple app that text from user input and updates the displayed text when the user hits "Update!"  A sample of how the UI changes is shown below:

This is how the UI looks initially.

![Image1](raw/master/UpdateText1.png)

The user enters "Hello!"

![Image1](raw/master/UpdateText2.png)

After hitting "Update!" the new displayed text is now "Hello!"

![Image1](raw/master/UpdateText3.png)