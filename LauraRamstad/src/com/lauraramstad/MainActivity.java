package com.lauraramstad;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {
	
	Button.OnClickListener updateButtonListener = 
			new Button.OnClickListener() {
			@Override
			public void onClick(View v) {
				update();
			}

		};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        
		Button btn = (Button) findViewById(R.id.update_button);
		btn.setOnClickListener(updateButtonListener);  
        return true;
    }
    
    void update(){
    	EditText newText = (EditText) findViewById(R.id.enter_text);
    	TextView oldText = (TextView) findViewById(R.id.display_text);
    	oldText.setText(newText.getText());
    	newText.setText(null);
    	
    }
}

